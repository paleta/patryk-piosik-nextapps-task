import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'summary', loadChildren: './summary/summary.module#SummaryModule' },
  { path: 'cart-and-products', loadChildren: './cart-and-products/cart-and-products.module#CartAndProductsModule' },
  { path: 'churn', loadChildren: './churn/churn.module#ChurnModule' },
  { path: 'clients', loadChildren: './clients/clients.module#ClientsModule' },
  { path: 'overview', loadChildren: './overview/overview.module#OverviewModule' },
  { path: 'quality-management', loadChildren: './quality-management/quality-management.module#QualityManagementModule' },
  { path: 'sale', loadChildren: './sale/sale.module#SaleModule' },
  {
    path: '**',
    redirectTo: '/summary',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
