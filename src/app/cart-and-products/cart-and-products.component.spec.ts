import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartAndProductsComponent } from './cart-and-products.component';

describe('CartAndProductsComponent', () => {
  let component: CartAndProductsComponent;
  let fixture: ComponentFixture<CartAndProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartAndProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartAndProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
