import { SharedModule } from './../shared/shared.module';
import { CartAndProductsRoutingModule } from './cart-and-products-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartAndProductsComponent } from './cart-and-products.component';

@NgModule({
  declarations: [CartAndProductsComponent],
  imports: [
    CartAndProductsRoutingModule,
    SharedModule
  ]
})
export class CartAndProductsModule { }
