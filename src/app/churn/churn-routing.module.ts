import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ChurnComponent } from './churn.component';

const routes: Routes = [
    { path: '', component: ChurnComponent },
];

@NgModule({
    declarations: [],
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ],
    providers: [],
})
export class ChurnRoutingModule { }
