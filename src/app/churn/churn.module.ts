import { NgModule } from '@angular/core';
import { ChurnComponent } from './churn.component';
import { ChurnRoutingModule } from './churn-routing.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [ChurnComponent],
  imports: [
    ChurnRoutingModule,
    SharedModule
  ]
})
export class ChurnModule { }
