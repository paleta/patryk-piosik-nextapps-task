import { NgModule } from '@angular/core';
import { ClientsComponent } from './clients.component';
import { ClientsRoutingModule } from './clients-routing.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [ClientsComponent],
  imports: [
    ClientsRoutingModule,
    SharedModule,
  ]
})
export class ClientsModule { }
