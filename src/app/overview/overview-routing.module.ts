import { OverviewComponent } from './overview.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
    { path: '', component: OverviewComponent },
];

@NgModule({
    declarations: [],
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ],
    providers: [],
})
export class OverviewRoutingModule { }
