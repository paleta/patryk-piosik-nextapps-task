import { NgModule } from '@angular/core';
import { OverviewComponent } from './overview.component';
import { SharedModule } from '../shared/shared.module';
import { OverviewRoutingModule } from './overview-routing.module';

@NgModule({
  declarations: [OverviewComponent],
  imports: [
    OverviewRoutingModule,
    SharedModule
  ]
})
export class OverviewModule { }
