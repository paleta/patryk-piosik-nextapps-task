import { QualityManagementComponent } from './quality-management.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';


const routes: Routes = [
    { path: '', component: QualityManagementComponent },
];

@NgModule({
    declarations: [],
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ],
    providers: [],
})
export class QualityManagementRoutingModule { }
