import { NgModule } from '@angular/core';
import { QualityManagementComponent } from './quality-management.component';
import { QualityManagementRoutingModule } from './quality-management-routing.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [QualityManagementComponent],
  imports: [
    QualityManagementRoutingModule,
    SharedModule,
  ]
})
export class QualityManagementModule { }
