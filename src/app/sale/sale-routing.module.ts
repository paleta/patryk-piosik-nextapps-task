import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { SaleComponent } from './sale.component';

const routes: Routes = [
    { path: '', component: SaleComponent },
];

@NgModule({
    declarations: [],
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ],
    providers: [],
})
export class SaleRoutingModule { }
