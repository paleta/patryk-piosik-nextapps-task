import { NgModule } from '@angular/core';
import { SaleComponent } from './sale.component';
import { SaleRoutingModule } from './sale-routing.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [SaleComponent],
  imports: [
    SaleRoutingModule,
    SharedModule
  ]
})
export class SaleModule { }
