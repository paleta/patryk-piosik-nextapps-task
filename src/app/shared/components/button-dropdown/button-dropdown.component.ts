import { Component, OnInit, Input } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-button-dropdown',
  templateUrl: './button-dropdown.component.html',
  styleUrls: ['./button-dropdown.component.scss'],
  animations: [
    trigger('rotate', [
        state('closed', style({ transform: 'rotate(0)' })),
        state('opened', style({ transform: 'rotate(-180deg)' })),
        transition('closed => opened', animate('200ms ease-out')),
        transition('opened => closed', animate('200ms ease-in'))
    ])
  ]

})
export class ButtonDropdownComponent implements OnInit {

  @Input() opened = false;

  constructor() { }

  ngOnInit() {
  }

}
