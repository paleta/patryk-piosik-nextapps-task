import { Directive, ElementRef, OnInit, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[appPositiveOrNegative]'
})
export class PositiveOrNegativeDirective implements AfterViewInit {

  constructor(private el: ElementRef<HTMLDivElement>) {
  }

  ngAfterViewInit() {
    const value = this.el.nativeElement.innerText;
    console.log(value);
    this.el.nativeElement.style.color = /\+/gmi.test(value) ? 'green' : 'red';
  }
 
}
