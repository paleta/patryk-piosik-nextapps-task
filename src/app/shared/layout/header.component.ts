import { Component, OnInit, Output, EventEmitter } from '@angular/core';


@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    opened = false;
    @Output() triggerSidebar: EventEmitter<void> = new EventEmitter();

    constructor() { }

    ngOnInit(): void {

    }

    toggleSidebar() {
        this.triggerSidebar.emit();
    }
}
