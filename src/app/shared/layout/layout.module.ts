import { RouterModule } from '@angular/router';
import { MaterialModule } from './../material.module';
import { FooterComponent } from './footer.component';
import { HeaderComponent } from './header.component';
import { LayoutComponent } from './layout.component';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared.module';
import { SidebarComponent } from './sidebar.component';
import { LayoutService } from './layout.service';


@NgModule({
    declarations: [
        LayoutComponent,
        HeaderComponent,
        FooterComponent,
        SidebarComponent,
    ],
    imports: [
        SharedModule,
        MaterialModule,
        RouterModule,
    ],
    exports: [
        LayoutComponent
    ],
    providers: [
        LayoutService,
    ],
})
export class LayoutModule { }
