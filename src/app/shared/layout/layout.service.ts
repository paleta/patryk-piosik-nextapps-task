import { Injectable } from '@angular/core';

@Injectable()
export class LayoutService {

    private mainNavItems = [
        { id: 1, display: 'Podsumowanie', path: ['/', 'summary'], iconName: 'home' },
        { id: 2, display: 'Przegląd kampanii', path: ['/', 'overview'], iconName: 'hearing' },
    ];

    private raportItems = [
        { id: 1, display: 'Sprzedaż', path: ['/', 'sale'], iconName: 'attach_money' },
        { id: 2, display: 'Koszyk i produkty', path: ['/', 'cart-and-products'], iconName: 'local_grocery_store' },
        { id: 3, display: 'Klienci', path: ['/', 'clients'], iconName: 'perm_contact_calendar' },
        { id: 4, display: 'Churn', path: ['/', 'churn'], iconName: 'call_split' },
        { id: 5, display: 'Zarządzanie jakością', path: ['/', 'quality-management'], iconName: 'record_voice_over' },
    ];

    getMainNavItems() {
        return this.mainNavItems.slice();
    }

    getRaportItems() {
        return this.raportItems.slice();
    }

}
