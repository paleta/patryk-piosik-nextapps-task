import { LayoutService } from './layout.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @Input() opened = true;

  mainNavItems;
  raportItems;

  constructor(private layoutService: LayoutService) { }

  ngOnInit() {
    this.mainNavItems = this.layoutService.getMainNavItems();
    this.raportItems = this.layoutService.getRaportItems();
  }

}
