import { MaterialModule } from './material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonDropdownComponent } from './components/button-dropdown/button-dropdown.component';
import { PositiveOrNegativeDirective } from './directives/positive-or-negative.directive';

@NgModule({
  declarations: [ButtonDropdownComponent, PositiveOrNegativeDirective],
  imports: [
    CommonModule,
    MaterialModule,
  ],
  exports: [
    CommonModule,
    ButtonDropdownComponent,
    PositiveOrNegativeDirective
  ]
})
export class SharedModule { }
