import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-summary-label',
  templateUrl: './label.component.html',
  styleUrls: ['./label.component.scss']
})
export class LabelComponent implements OnInit {

  @Input() title;
  @Input() value;
  @Input() prec;
  @Input() active = false;

  constructor() { }

  ngOnInit() {
  }

}
