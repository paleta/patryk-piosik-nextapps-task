import { Component, OnInit, OnDestroy } from '@angular/core';
import { SummaryService } from './summary.service';
import { Observable, Subject } from 'rxjs';
import { shareReplay, takeUntil, reduce, map } from 'rxjs/operators';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss'],
  animations: [
    trigger('enterTrigger', [
    state('fadeIn', style({
        opacity: '1',
        transform: 'translateY(50%)'
    })),
    transition('void => *', [style({ opacity: '0' }), animate('300ms')])
    ])
  ]
})
export class SummaryComponent implements OnInit, OnDestroy {

  opened = false;
  unsub$ = new Subject();
  summaryLablesArray$: Observable<any[]>;

  activeLabel = '';

  constructor(private summaryService: SummaryService) { }

  ngOnInit() {
    this.summaryLablesArray$ = this.summaryService.getSummaryLabels().pipe(map(summaryLabels => {
      return Object.keys(summaryLabels).map(key => ({ ...summaryLabels[key], id: key }));
    }), takeUntil(this.unsub$), shareReplay());

    // takeUntil - żeby zakończyć 'shareReplay()' - z powodu bug'a
    // INFO: https://blog.strongbrew.io/share-replay-issue/
  }

  changeLabel(id: string) {
    this.activeLabel = id;
  }

  ngOnDestroy() {
    this.unsub$.next();
    this.unsub$.complete();
  }

}
