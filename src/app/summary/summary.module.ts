import { MaterialModule } from './../shared/material.module';
import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { SummaryComponent } from './summary.component';
import { SummaryRoutingModule } from './summary-routing.module';
import { LabelComponent } from './components/label/label.component';
import { SummaryService } from './summary.service';
import { LineChartComponent } from './components/line-chart/line-chart.component';

@NgModule({
  declarations: [SummaryComponent, LabelComponent, LineChartComponent],
  imports: [
    SummaryRoutingModule,
    SharedModule,
    MaterialModule,
  ],
  providers: [SummaryService]
})
export class SummaryModule { }
