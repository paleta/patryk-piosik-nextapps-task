import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

export interface Stock {
    date: Date;
    value: number;
}

@Injectable()
export class SummaryService {

    private summaryLabels$: Observable<object> = of({
        sale: { title: 'Sprzedaż', value: '2 429 305 zł', prec: '+5,16%', additionalInfo: 'vs poprzedni okres', stocks: [
            { date: new Date('2015-03-27'), value: 614.48 },
            { date: new Date('2015-03-28'), value: 617.62 },
            { date: new Date('2015-03-29'), value: 609.86 },
            { date: new Date('2015-03-30'), value: 399.55 },
            { date: new Date('2015-04-02'), value: 618.63 },
            { date: new Date('2015-04-03'), value: 429.32 },
            { date: new Date('2015-04-04'), value: 624.31 },
            { date: new Date('2015-04-05'), value: 633.68 },
            { date: new Date('2015-04-09'), value: 536.23 },
            { date: new Date('2015-04-10'), value: 628.44 },
            { date: new Date('2015-04-11'), value: 626.20 },
            { date: new Date('2015-04-12'), value: 622.77 },
            { date: new Date('2015-04-13'), value: 605.23 },
            { date: new Date('2015-04-16'), value: 680.13 },
            { date: new Date('2015-04-17'), value: 609.70 },
            { date: new Date('2015-04-18'), value: 608.34 },
            { date: new Date('2015-04-19'), value: 787.44 },
            { date: new Date('2015-04-20'), value: 572.98 },
            { date: new Date('2015-04-23'), value: 571.70 },
            { date: new Date('2015-04-24'), value: 260.28 },
            { date: new Date('2015-04-25'), value: 610.00 },
            { date: new Date('2015-04-26'), value: 607.70 },
            { date: new Date('2015-04-27'), value: 603.00 },
            { date: new Date('2015-04-30'), value: 555.98 },
            { date: new Date('2015-05-01'), value: 582.13 },
        ]},
        clients: { title: 'L. klientów', value: '73 295', prec: '-7,92%', stocks: [
            { date: new Date('2016-03-27'), value: 314.48 },
            { date: new Date('2016-03-28'), value: 317.62 },
            { date: new Date('2016-03-29'), value: 609.86 },
            { date: new Date('2016-03-30'), value: 599.55 },
            { date: new Date('2016-04-02'), value: 618.63 },
            { date: new Date('2016-04-03'), value: 529.32 },
            { date: new Date('2016-04-04'), value: 624.31 },
            { date: new Date('2016-04-05'), value: 733.68 },
            { date: new Date('2016-04-09'), value: 636.23 },
            { date: new Date('2016-04-10'), value: 428.44 },
            { date: new Date('2016-04-11'), value: 626.20 },
            { date: new Date('2016-04-12'), value: 622.77 },
            { date: new Date('2016-04-13'), value: 605.23 },
            { date: new Date('2016-04-16'), value: 280.13 },
            { date: new Date('2016-04-17'), value: 109.70 },
            { date: new Date('2016-04-18'), value: 708.34 },
            { date: new Date('2016-04-19'), value: 587.44 },
            { date: new Date('2016-04-20'), value: 572.98 },
            { date: new Date('2016-04-23'), value: 571.70 },
            { date: new Date('2016-04-24'), value: 860.28 },
            { date: new Date('2016-04-25'), value: 610.00 },
            { date: new Date('2016-04-26'), value: 907.70 },
            { date: new Date('2016-04-27'), value: 603.00 },
            { date: new Date('2016-04-30'), value: 583.98 },
            { date: new Date('2016-05-01'), value: 582.13 },

        ]},
        cart: { title: 'Śr. koszyk', value: '13,82 zł', prec: '+21,41%', stocks: [
            { date: new Date('2017-03-27'), value: 114.48 },
            { date: new Date('2017-03-28'), value: 617.62 },
            { date: new Date('2017-03-29'), value: 609.86 },
            { date: new Date('2017-03-30'), value: 599.55 },
            { date: new Date('2017-04-02'), value: 618.63 },
            { date: new Date('2017-04-03'), value: 229.32 },
            { date: new Date('2017-04-04'), value: 624.31 },
            { date: new Date('2017-04-05'), value: 633.68 },
            { date: new Date('2017-04-09'), value: 836.23 },
            { date: new Date('2017-04-10'), value: 628.44 },
            { date: new Date('2017-04-11'), value: 426.20 },
            { date: new Date('2017-04-12'), value: 622.77 },
            { date: new Date('2017-04-13'), value: 605.23 },
            { date: new Date('2017-04-16'), value: 580.13 },
            { date: new Date('2017-04-17'), value: 609.70 },
            { date: new Date('2017-04-18'), value: 508.34 },
            { date: new Date('2017-04-19'), value: 587.44 },
            { date: new Date('2017-04-20'), value: 572.98 },
            { date: new Date('2017-04-23'), value: 571.70 },
            { date: new Date('2017-04-24'), value: 560.28 },
            { date: new Date('2017-04-25'), value: 610.00 },
            { date: new Date('2017-04-26'), value: 607.70 },
            { date: new Date('2017-04-27'), value: 603.00 },
            { date: new Date('2017-04-30'), value: 583.98 },
            { date: new Date('2017-05-01'), value: 982.13 },

        ]},
        churn: { title: 'Churn rate', value: '35,94%', prec: '+4,03%', stocks: [
            { date: new Date('2018-03-27'), value: 614.48 },
            { date: new Date('2018-03-28'), value: 217.62 },
            { date: new Date('2018-03-29'), value: 209.86 },
            { date: new Date('2018-03-30'), value: 299.55 },
            { date: new Date('2018-04-02'), value: 218.63 },
            { date: new Date('2018-04-03'), value: 629.32 },
            { date: new Date('2018-04-04'), value: 624.31 },
            { date: new Date('2018-04-05'), value: 633.68 },
            { date: new Date('2018-04-09'), value: 636.23 },
            { date: new Date('2018-04-10'), value: 628.44 },
            { date: new Date('2018-04-11'), value: 326.20 },
            { date: new Date('2018-04-12'), value: 322.77 },
            { date: new Date('2018-04-13'), value: 305.23 },
            { date: new Date('2018-04-16'), value: 580.13 },
            { date: new Date('2018-04-17'), value: 609.70 },
            { date: new Date('2018-04-18'), value: 608.34 },
            { date: new Date('2018-04-19'), value: 587.44 },
            { date: new Date('2018-04-20'), value: 872.98 },
            { date: new Date('2018-04-23'), value: 871.70 },
            { date: new Date('2018-04-24'), value: 860.28 },
            { date: new Date('2018-04-25'), value: 610.00 },
            { date: new Date('2018-04-26'), value: 607.70 },
            { date: new Date('2018-04-27'), value: 603.00 },
            { date: new Date('2018-04-30'), value: 583.98 },
            { date: new Date('2018-05-01'), value: 582.13 },

        ]},
    });

    getSummaryLabels() {
        return this.summaryLabels$;
    }
}
